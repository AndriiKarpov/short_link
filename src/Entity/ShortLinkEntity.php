<?php

namespace Drupal\short_link\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Short Link entity.
 *
 * @ingroup short_link
 *
 * @ContentEntityType(
 *   id = "short_link",
 *   label = @Translation("Short Link"),
 *   base_table = "short_link",
 *   translatable = FALSE,
 *   admin_permission = "administer short link entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *   }
 * )
 */
class ShortLinkEntity extends ContentEntityBase implements ShortLinkEntityInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function getOriginalLink() {
    return $this->get('original_link')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setOriginalLink($original_link) {
    $this->set('original_link', $original_link);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getShortLink() {
    return $this->get('short_link')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setShortLink($short_link) {
    $this->set('short_link', $short_link);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($created) {
    $this->set('created', $created);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setReadOnly(TRUE);

    $fields['original_link'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Original Link'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
      ]);

    $fields['short_link'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Original Link'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 64,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was last created.'));

    return $fields;
  }

}
