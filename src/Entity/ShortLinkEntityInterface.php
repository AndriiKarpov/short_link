<?php

namespace Drupal\short_link\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Short Link entities.
 *
 * @ingroup short_link
 */
interface ShortLinkEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Gets the original link.
   *
   * @return string
   *   Original Link.
   */
  public function getOriginalLink();

  /**
   * Sets the creation date of the Short Link.
   *
   * @param string $original_link
   *   The generated Short Link.
   *
   * @return $this
   *   The class instance that this method is called on.
   */
  public function setOriginalLink($original_link);

  /**
   * Gets the short link.
   *
   * @return string
   *   Short link.
   */
  public function getShortLink();

  /**
   * Sets the original link.
   *
   * @param string $short_link
   *   Original link.
   *
   * @return $this
   *   The class instance that this method is called on.
   */
  public function setShortLink($short_link);

  /**
   * Gets the short link creation timestamp.
   *
   * @return int
   *   Creation timestamp of the short link.
   */
  public function getCreatedTime();

  /**
   * Sets the creation date of the short link.
   *
   * @param int $created
   *   The timestamp of when the short link was created.
   *
   * @return $this
   *   The class instance that this method is called on.
   */
  public function setCreatedTime($created);

}
