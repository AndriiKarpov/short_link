<?php

namespace Drupal\short_link;

/**
 * Class ShortLinkService.
 */
class ShortLinkService implements ShortLinkServiceInterface {

  /**
   * {@inheritdoc}
   */
  public function idToBase62(int $id): string {
    $map = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    $short_url = '';

    while ($id > 0) {
      $short_url .= $map[$id % 62];
      $id = intval($id / 62);
    }

    return strrev($short_url);
  }

  /**
   * {@inheritdoc}
   */
  public function base62ToInt(string $url): int {
    $id = 0;

    for ($i = 0; $i < strlen($url); $i++) {
      if ('a' <= $url[$i] && $url[$i] <= 'z') {
        $id = $id * 62 + ord($url[$i]) - ord('a');
      }
      if ('A' <= $url[$i] && $url[$i] <= 'Z') {
        $id = $id * 62 + ord($url[$i]) - ord('A') + 26;
      }
      if ('0' <= $url[$i] && $url[$i] <= '9') {
        $id = $id * 62 + ord($url[$i]) - ord('0') + 52;
      }
    }
    return $id;
  }

}
