<?php

namespace Drupal\short_link;

/**
 * Interface ShortLinkServiceInterface.
 */
interface ShortLinkServiceInterface {

  /**
   * Short Link.
   *
   * @param int $id
   *   The generated id.
   *
   * @return $this
   *   The class instance that this method is called on.
   */
  public function idToBase62(int $id): string;

  /**
   * ID.
   *
   * @param string $url
   *   The original url.
   *
   * @return $this
   *   The class instance that this method is called on.
   */
  public function base62ToInt(string $url): int;

}
