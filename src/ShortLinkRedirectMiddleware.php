<?php

namespace Drupal\short_link;

use Drupal\Core\Database\Connection;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Short link middleware.
 */
class ShortLinkRedirectMiddleware implements HttpKernelInterface {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The kernel.
   *
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */
  protected $httpKernel;

  /**
   * Constructs the FirstMiddleware object.
   *
   * @param \Symfony\Component\HttpKernel\HttpKernelInterface $http_kernel
   *   The decorated kernel.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(HttpKernelInterface $http_kernel, Connection $connection) {
    $this->httpKernel = $http_kernel;
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public function handle(Request $request, $type = self::MASTER_REQUEST, $catch = TRUE) {
    $uri = $request->getRequestUri();
    $uri = ltrim($uri, '/');
    $link_record = $this->getLinkRecord($uri);
    if ($link_record) {
      return new RedirectResponse($link_record['original_link']);
    }

    return $this->httpKernel->handle($request, $type, $catch);
  }

  /**
   * {@inheritdoc}
   */
  public function getLinkRecord($uri) {
    return $this->connection->query("SELECT * FROM {short_link} WHERE short_link = :uri", [':uri' => $uri])->fetchAssoc();
  }

}
