<?php

namespace Drupal\short_link\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ShortLinkForm.
 */
class ShortLinkForm extends FormBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Component\Datetime\TimeInterface definition.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Drupal\short_link\ShortLinkServiceInterface definition.
   *
   * @var \Drupal\short_link\ShortLinkServiceInterface
   */
  protected $urlShortener;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->time = $container->get('datetime.time');
    $instance->urlShortener = $container->get('short_link.coder');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'short_link_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Url'),
      '#maxlength' => 255,
      '#weight' => '0',
      '#requred' => TRUE,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $url = $form_state->getValue('url');
    if (!$url || !filter_var($url, FILTER_VALIDATE_URL)) {
      $form_state->setErrorByName('url', $this->t('Invalid URL'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\short_link\Entity\ShortLinkEntityInterface $short_link */
    $short_link = $this->entityTypeManager->getStorage('short_link')->create([
      'original_link' => $form_state->getValue('url'),
      'created' => $this->time->getCurrentTime(),
    ]);
    $short_link->save();
    $short_link->setShortLink($this->urlShortener->idToBase62($short_link->id()));
    $short_link->save();

    $form_state->setRedirect('short_link.short_link_information', [
      'link' => $short_link->getShortLink(),
    ]);
  }

}
