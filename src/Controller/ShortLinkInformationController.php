<?php

namespace Drupal\short_link\Controller;

use Drupal\Core\Controller\ControllerBase;
use Endroid\QrCode\QrCode;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ShortLinkInformationController.
 */
class ShortLinkInformationController extends ControllerBase {

  /**
   * Drupal\Core\Datetime\DateFormatterInterface definition.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Drupal\short_link\ShortLinkServiceInterface definition.
   *
   * @var \Drupal\short_link\ShortLinkServiceInterface
   */
  protected $urlShortener;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->urlShortener = $container->get('short_link.coder');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function link($link) {
    /** @var \Drupal\short_link\Entity\ShortLinkEntityInterface $link_entity */
    $id = $this->urlShortener->base62ToInt($link);
    $link_entity = $this->entityTypeManager()->getStorage('short_link')->load($id);

    if (is_null($link_entity)) {
      throw new NotFoundHttpException();
    }

    $headers = [
      $this->t('Short Link ID'),
      $this->t('Original Link'),
      $this->t('Created Date'),
      $this->t('QR Code'),
    ];

    $qr_code = new QrCode($link_entity->getOriginalLink());
    $qr_code->setSize(50);

    $rows[] = [
      $link_entity->getShortLink(),
      $link_entity->getOriginalLink(),
      $this->dateFormatter->format($link_entity->getCreatedTime()),
      [
        'data' => [
          '#theme' => 'image',
          '#uri' => $qr_code->writeDataUri(),
        ],
      ],
    ];

    return [
      '#type' => 'table',
      '#header' => $headers,
      '#rows' => $rows,
    ];
  }

}
